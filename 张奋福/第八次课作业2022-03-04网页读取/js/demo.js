//启用服务器
let http = require("http");
let fs=require("fs");


//传统方法创建一个文件并且写入一个文件
let fd=fs.writeFileSync(`./index.html`,'我是第一个url访问的地址的内容',{flag:"a",autoClose:true});
//创建一个web服务器
let server=http.createServer((req,res)=>{
    //链接的url地址


    //字符转换编码
    res.setHeader("Content-Type","text/html;charset=utf-8");
    // 根据url的不同地址
    if(req.url==`./index.html`){
        //如果地址是一个文件那就读取这个文件的内容

        let cd=fs.readFileSync('./index.html');
   
        //在网页上打印它
        res.end();
    }
});

//链接的网址

server.on('connection',function(){
    console.log('我链接了');
})
server.listen(8080)
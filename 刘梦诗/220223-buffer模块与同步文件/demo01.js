const fs = require('fs');

const args = process.argv.slice(2);
const startingNumber = args[0];
if(!startingNumber){
    console.log('添加了一个文件');
    return
}

const isFolder = fileName => {
    return !fs.lstatSync(fileName).isFile()
  }
   
  const folders = fs
    .readdirSync('./')
    .map(fileName => {
      return fileName
    })
    .filter(isFolder)
   
  folders.map(folder => {
    const result = folder.match(/(\d)+/)
    if (result !== null) {
      const num = parseInt(result[0])
      if (num >= startingNumber) {
        fs.renameSync(folder, folder.split(num).join(num + 1))
      }
    }
  })
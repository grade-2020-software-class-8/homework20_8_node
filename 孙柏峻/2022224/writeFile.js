const fs=require('fs');
const randomName=require('chinese-random-name');
const { error } = require('console');
let random_name=[];

let file={
    //创建随机数
    random:function random(fileCount){
        
        let random_namearr=[]
        for (let i = 1; i <= fileCount; i++) {
            random_namearr[i]=randomName.generate();
            random_name[i-1]=random_namearr[i];
        }
        console.log(random_name);
    },
    //批量生成txt文件
    create_File:function create_File(fileCount){
        file.random(fileCount)
        for (let i = 1; i <= fileCount; i++) {
            fs.openSync(`${__dirname}/${i}.txt`,'w')
        }
    },
    //单独精准写入文件
    write_File:function write_File(fileCount,fileName){
        file.random(fileName)
        let fd=fs.openSync(`${__dirname}/${fileName}.txt`,'w')
        fs.writeSync(fd,`${random_name[fileName-1]}`,"utf8")
        fs.closeSync(fd);
        
        
    },
    //批量写入文件
    write_Files:function write_Files(fileCount){
        file.random(fileCount)
        for (let i = 1; i <= fileCount; i++) {
            let fd=fs.openSync(`${__dirname}/${i}.txt`,'w')
            fs.writeSync(fd,`${random_name[i-1]}`,"utf8")
            fs.closeSync(fd);
        }
    },
    handle:function handle(fileCount,fileName,funcname){
        if(isNaN(fileCount)||isNaN(fileName)){
            throw new Error("请输入数字");
        }
        return file[funcname](fileCount,fileName)
    }

}



module.exports={
    handle:file.handle,
    write_Files:file.write_Files,
    write_File:file.write_File,
    random:file.random,
    create_File:file.create_File
}


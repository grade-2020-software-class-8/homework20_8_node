const { buffer } = require("stream/consumers");

//Buffer 和字符串之间的转换时；
let string = "我 study node";
let bf = Buffer.from(string);
//  console.log(bf); 得出：<Buffer e6 88 91 20 73 74 75 64 79 20 6e 6f 64 65>
console.log(string.length);
console.log(bf.length);

//300二进制 1001，0110，0，当超过255时，会做截断，截断规则从后往前8位
//  Buffer.alloc(size[, fill[, encoding]])： 返回一个指定大小的 Buffer 实例，如果没有设置 fill，则默认填满 0
// 下面表示创建一个长度为 100、且用 0 填充的 Buffer。
let bfBuiler = Buffer.alloc(100);
console.log(bfBuiler);

//buffer与js的数组性能对比
//当前的毫秒数
let countNum =10000000;
let startAt =Date.now();
let bufferObj = Buffer.alloc(countNum); 
for(let i =0;i<countNum;i++){
    bufferObj[i]=255;


}
let endAt=Date.now();
console.log("buffer的耗时："+(endAt-startAt));

//数组形式
let numArr=[];
let startAt2 = Date.now();
for(let i=0;i<countNum;i++){
    numArr[i]=255;
    
}

let endAt2 = Date.now();
console.log("数组所耗费的时长是："+(endAt2-startAt2));

/****************_____________________________________________________ */
//fs :file system

 //同步文件的写入
// fs.open(path, flags[, mode], callback) 打开文件
let fileSystem = require('fs');
// path - 文件的路径。
// flags - 文件打开的行为。具体值详见下文
// mode - 设置文件模式(权限)，文件创建默认权限为 0666(可读，可写)。
// callback - 回调函数，带有两个参数如：callback(err, fd)。

let fd =fileSystem.openSync("./input.txt","w");
console.log(fd);

//二 往这个文件描述符中写入
 // fs.writeFile(file, data[, options], callback)
 // file 就是文件描述符 data 要输入的内容 options
fileSystem.writeSync(fd,"2022-02-24学习了fs的文件写入",20);

//三 文件的关闭
 //关闭文件 closeSync(fd: number)
 // fd就是文件描述符，就是openSync的返回值
 fileSystem.closeSync(fd)

let http = require("http");
let fs = require('fs');
let nunjucks = require("nunjucks");
const Logincontroller = require("./home/controller/userController");

let session = [];
session['123'] = { 'name': 'zhangsan', age: 20 };

http.createServer(function (req, res) {


    let url = req.url;
    // console.log(req.headers.cookie);

    /**接受参数 */
    
    let postData ='';
    req.on('data', function (data) {//异步模块
        
        postData += data;
        
    })
    

    /**
     * 监听流文件读取成功
     * 
     */
    req.on('end', async function () {
        
        if (url.lastIndexOf(".") > -1) {
            let path = '.' + url;
            console.log(path);
            
            fs.exists(path, function (result) {
                if (!result) {
                    path = './home/imgs/404.png';
                }
                fs.readFile(path, function (err, data) {
                    if (err) {
                        res.end(err.message);
                    } else {
                        res.write(data);
                        res.end();
                    }
                })

            })

        } else {

            //取出cookie,把cookie转成 key=val 的数组
            console.log(req.headers.cookie);
            if(req.headers.cookie){
                let cookieStrArr = req.headers.cookie.split("; ");
                let cookies = {};
                cookieStrArr.forEach(element => {
                    let cookieKeyAndVal = element.split('=');
                    cookies[cookieKeyAndVal[0]] = cookieKeyAndVal[1];
                });
                req.cookies = cookies;//保存到请求对象
            }


            res.setHeader("Content-type", "text/html;charset=utf8");
            
            //处理url
            let query = url.split('?')[1];
            let queryArry = query.split('&');
            let needArr = [];
            for (let val in queryArry) {
                let temArr = queryArry[val].split('=');
                needArr[temArr[0]] = temArr[1];
            }
            req.query = needArr;


            //处理post的数据
        
            let postArr = {};//把query参数构建成key val格式 如 c=index&a=index 构建成 [ c: 'index', a: 'index' ]
            if (req.method == "POST" && postData) {
                let postString = postData.split('&');//得到数据 [c=index,a=index];
            
                for (let postKey in postString) {
                    let temArr2 = postString[postKey].split('=');
                    postArr[temArr2[0]] = temArr2[1];
                }
                req.post = postArr;
            }
           
            let controllerName = needArr['c'];
            controllerName = controllerName.replace(controllerName[0], controllerName[0].toUpperCase()) + 'Controller' //index

            let path = './home/controller/' + controllerName;
            
            let controller = require(path);
            let action = needArr['a'];
            try{
                let obj = new controller(req, res, nunjucks);// 创建了对象  
                let result = await obj[action]();
                res.write(result);//发送给客户端
                // res.setHeader('cookie',['demo=123456','password=654321']);
                res.end();
            }catch(err){
                console.log(err);
            }
           

            //res.end();
        }

    })



}).listen(8080);

async function handle(obj, action) {
    let result = await obj[action]();
    return result;
}



 

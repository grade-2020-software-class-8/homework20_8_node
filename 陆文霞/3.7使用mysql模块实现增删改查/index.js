
const mysql = require('mysql');

//配置
let db = mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'root',
    port:'3306',
    database:'demo'
});

//封装数据库操作
function query(sql , callback){
    db.connect();//建立连接
    db.query(sql, function(err , result){
        if(err){
            console.log(err.message);
            return;
        }
        callback(result);
    })
    db.end();//释放资源

}

//创建表格
let sql = `CREATE TABLE info
( id int primary key,
name varchar(25) not null,
age int not null)`;
query(sql,function(result){
    if(result){
        console.log('表格创建成功');
    }
    });

//增加字段
let sql1 = `insert into info(id,name,age) values
(1,'小张',18),
(2,'小明',19),
(3,'小红',20),
(4,'小黄',17)`;
query(sql1 , function(result){
    if(result){
        console.log('数据插入成功');
    }
})
查询字段
let sql2 =`select * from info`;
query(sql2,function(result){
    console.log(result);
})

修改字段
let sql3= `update info set name='小黑' where id=1`;
query(sql3,function(result){
    if(result){
        console.log('修改成功');
    }
});

//删除字段
let sql4 = `DELETE FROM info where name='小黑'`;
query(sql4,function(result){
    if(result){
        console.log('删除成功');
    }
})


let fs =require('fs');

//创建目录
let path = './files';
//判断目录是否存在
if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
}

for(let i = 1 ;i <= 100 ; i++){
    //创建文件名
    let filename = path + '/' + i + '.txt';
    //文件内容随机数
    let num = Math.floor(Math.random()*100);
    //打开文件
    let fd = fs.openSync(filename,'a');
    //文件写入
    fs.writeFileSync(fd,num);
    //关闭文件
    fs.closeSync(fd);
}

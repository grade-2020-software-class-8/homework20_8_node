CREATE SCHEMA `yiqing` ;
CREATE TABLE `yiqing`.`number` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) NOT NULL,
  `nums` INT NOT NULL,
  `date` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
  alter table number convert to character set utf8mb4 collate utf8mb4_bin;
  
INSERT INTO `yiqing`.`number` (`city`, `nums`, `date`) VALUES ('龙岩', '1', '2022-03-16');
INSERT INTO `yiqing`.`number` (`city`, `nums`, `date`) VALUES ('泉州', '36', '2022-03-16');
INSERT INTO `yiqing`.`number` (`city`, `nums`, `date`) VALUES ('漳州', '2', '2022-03-15');
INSERT INTO `yiqing`.`number` (`city`, `nums`, `date`) VALUES ('北京', '11', '2022-03-15');


CREATE SCHEMA `user` ;
CREATE TABLE `user`.`userinfo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
  alter table userinfo convert to character set utf8mb4 collate utf8mb4_bin;
  
INSERT INTO `user`.`userinfo` (`username`, `password`) VALUES ('adim', '123456');


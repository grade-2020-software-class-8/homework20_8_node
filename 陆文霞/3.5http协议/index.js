const http = require('http');

const fs = require('fs');

let server = http.createServer();

server.on('request',function(req,res){
    let url =  req.url;
    if(url == "/"){
        res.end("success");
    }else if(url == "/css"){
        res.setHeader('Content-type','text/html; charset=utf-8');
        res.end(fs.readFileSync('./css/index.html'))
    }else if(url == "/img"){
        res.end(fs.readFileSync('./img/index.jpg'))
    }

});
server.listen(8080);

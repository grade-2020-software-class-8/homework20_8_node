let http =require('http');
let fs =require('fs');
const { index } = require('./home/controller');

http.createServer(function(req,res){
    let url =req.url;
     //静态资源
    if(url.lastIndexOf('.')>-1){
        let path ='.'+url;
        console.log(path);

        fs.exists(path,function(result){
            if(!result){
                path ='./home/imgs/404.png';
            }
            fs.readFile(path,function(err,data){
                if(err){
                    res.end(err.message);
                }else{
                    res.write(data);
                    res.end();
                }
            })
        })
    }else{
        //动态资源
        let url =req.url;//是 uri  /path?c=index&a=index 我们需要的是 query(c=index&a=index)
        // console.log(url);
        let query =url.split('?')[1];
        console.log('.....'+query);
        let queryArr =query.split('&');
        // console.log(queryArr);
        let needArr =[];
        for(let values in queryArr){
            let temArr = queryArr[values].split('=');
            needArr[temArr[0]]=temArr[1];
        }
        // console.log(needArr);
        req.query = needArr;

        let controllerName =needArr['c'];
        // console.log('controller-----'+controllerName);

        let path ='./home/controller/'+controllerName;//controller的路径地址
        // console.log(path);

        let controller =require(path);//引入controller ,变量 controller 就是 引入的 Index(jason格式)
        let action =needArr['a'];//要执行的函数,action是个变量

        // controller.action(req);//动态获取,action是变量时，json对象通过 [变量名] 
        controller.index(req,res);
        // console.log('index'+controller.index);
        // controller[index](req);
        //res.end();

    }
}).listen(8080,function(){
    console.log('http://127.0.0.1:8080');
});
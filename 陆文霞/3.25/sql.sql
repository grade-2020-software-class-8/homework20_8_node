CREATE SCHEMA `yiqing` ;
CREATE TABLE `yiqing`.`info` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `value` INT NOT NULL,
  `cureNum` INT NOT NULL,
  `deathNum` INT NOT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `yiqing`.`info` 
ADD UNIQUE INDEX `name_UNIQUE` (`name` ASC);

ALTER TABLE `yiqing`.`info` 
ADD COLUMN `time` VARCHAR(45) NOT NULL AFTER `deathNum`;
    alter table info convert to character set utf8mb4 collate utf8mb4_bin;
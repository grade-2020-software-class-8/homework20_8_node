class BaseController{
    constructor(req,res,nunjucks){
        this.req = req;

        this.res = res;

        this.nunjucks = nunjucks;

        this.nunjucks.configure('./home/html', { autoescape: true });//第一参数 就是指向我们的html 目录
    }
}

module.exports=BaseController;
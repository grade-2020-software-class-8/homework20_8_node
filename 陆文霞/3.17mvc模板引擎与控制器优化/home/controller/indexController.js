const Model = require("../model/Base");
const BaseController = require("./BaseController");

class IndexControll extends BaseController {
    async index(){
        //获取到url的传参?
        console.log(this.req.query);
        // 获取参数

        let date  = this.req.query['date'];
        console.log(date);
        let sql = 'select * from number where date=? order by nums desc';
        // try {
            let model =new Model();
            // let result = await model.query(sql, date);
            let result = await model.query(sql, date);
            console.log(result);
            
            return  this.nunjucks.render('demo.html',{result:result});

        // } catch (err) {
        //     console.log(err);
        // }
    }
}

module.exports=IndexControll;
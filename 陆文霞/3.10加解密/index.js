// 建一个表 user，里面有字段 name，password ，当然还可以加入别的字段，要求用node实现，写入数据，
// password 需要被md5加密。查询数据时候能通过明文，加密后查找到对应的数据。

let crypto =require('crypto');

let mysql =require('mysql');

let db =mysql.createConnection({
    host:'localhost',
    port:'3306',
    database:'user',
    user:'root',
    password:'root'
});

function query(sql,callback){
    db.connect();
    db.query(sql,function(err, result){
        if(err){
            console.log(err.message);
        }
        callback(result);
    })
    db.end();
}

//加密
function encrypt(password){
    let md5 =crypto.createHash('md5');
    md5.update(password);
    let pw = md5.digest('hex');
    return pw;
}
let password =encrypt('2333');

let sql = `INSERT INTO userinfo(username,password) VALUES ("admin1",'${password}')`;
query(sql,function(result){
    console.log('插入成功');
    console.log(result);
});
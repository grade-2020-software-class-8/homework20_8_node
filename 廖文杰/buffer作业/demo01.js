let fs = require('fs');

for (let i = 1; i <= 9; i++) {
    let fd = fs.openSync(`第${i}个文件`, `s`); //以读取模式打开文件，如果文件不存在则创建
    //异步写入
    fs.writeSync(fd, `han${i+1}`, function(err) {
        if (err) {
            throw err;
        }
    });
    fs.closeSync(fd);
}
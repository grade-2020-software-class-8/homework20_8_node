
// add , subtract , multiply and divide

let count = { 
	'+': function (a,b) { 
		return a+b;
	},
    '-':function(a,b){
        return a-b;
    },
    '*':function(a,b){
        return a*b;
    },
    '/':function(a,b){
        return a/b;
    },
    
}

function handle(a,b,funcname){
    if(isNaN(a)||isNaN(b)){
        throw new Error("请输入数字");
    }
    return count[funcname](a,b)
   

}

module.exports={
    handle:handle
}

let fs = require("fs");

function randomPrize() {
    let prize = ["你不知道的JavaScript", "Python从入门到精通", "Node.js深入浅出", "谢谢参与"]
    return prize[Math.floor(Math.random() * prize.length + 0)]
}


for (let i = 1; i < 7; i++) {
    fs.writeFile(`第${i}个文件.txt`, `${randomPrize()}`, err => {
        if (err) return console.log("写入文件失败", err);
        console.log("写入文件成功");
    })
}